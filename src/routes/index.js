import Router from 'vue-router'
import Vue from 'vue'

import HelloWorld from '@/components/HelloWorld.vue'
import About from '@/components/About.vue'

import Tarefas from '@/components/Tarefas.vue'

import routesTarefas from './tarefas'

Vue.use(Router)

const router = new Router({
  routes: [
    { path: '/', name: 'Hello', component: HelloWorld },
    { path: '/about', name: 'About', component: About },
    { path: '/tarefas', component: Tarefas, children: [...routesTarefas] }
  ],
  base: process.env.BASE_URL,
  mode: 'history'
})

export default router
